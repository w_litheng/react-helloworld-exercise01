var React = require('react');
var ReactDOM = require('react-dom');
var MyComponent = require('./MyComponent.jsx');

ReactDOM.render(
  <MyComponent/>,
  document.getElementById('app')
);