var gulp = require('gulp');
var gulpUtil = require('gulp-util');
var webpack = require('webpack');
var WebpackDevServer = require("webpack-dev-server");
var webpackConfig = require('../webpack.config.js');

gulp.task('watch', ['webpack'], function () {
  process.stdout.write('running watch task to run webpack task');
  gulp.watch(['./src/**/*'], ['webpack']);
});

gulp.task('webpack', function(done) {
    // run webpack
    webpack({
      entry: './src/main.js',

      output: {
         path:'./',
         filename: 'index.js',
      },

      devServer: {
         inline: true,
         port: 8080
      },

      module: {
         loaders: [
            {
               test: /\.jsx?$/,
               exclude: /node_modules/,
               loader: 'babel',

               query: {
                  presets: ['es2015', 'react']
               }
            }
         ]
      }
    }, function(error) {
        var pluginError;

        if (error) {
            pluginError = new gulpUtil.PluginError('webpack', error);

            if (done) {
                done(pluginError);
            } else {
                gulpUtil.log('[webpack]', pluginError);
            }

            return;
        }

        if (done) {
            done();
        }
    });
});

gulp.task("webpack-dev-server", function(callback) {
    // Start a webpack-dev-server
    new WebpackDevServer(webpack(webpackConfig), {
        // server and middleware options
        hot: true,
        historyApiFallback: false,
		stats: {
			colors: true
        }
    }).listen(8080, "localhost", function(err) {
        if(err) {
          throw new gulpUtil.PluginError("webpack-dev-server", err);
        }
        // Server listening
        gulpUtil.log("[webpack-dev-server]", "http://localhost:8080/webpack-dev-server");

        // keep the server alive or continue?
        // callback();
    });

});
